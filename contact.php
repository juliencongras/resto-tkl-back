<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>
    <link rel="stylesheet" href="style.css">
    
</head>
<body>
    <?php include 'H5.php';
    include "header3.php";
    ?>
    <?php include 'data.php'?>
    <?php session_start();?>
    <div class="container">
        <div class="text"><p>Adresse :<span class="center"><?=$cont[0]['adresse']?></span></p>
            <p>Télephone : <a href="tel:<?=$cont[0]['tel']?>" class="center"><?=$cont[0]['tel']?></a></p>
        </div>
        <iframe src=<?=$cont[0]['mapURL']?> width="450" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>

    <div class="boutonModifierPres"><?php 
    if($_SESSION['admin']){
        echo "<a href='formulaireContact.php?id=".$cont[$i]['id']."'>modifier</a>";
    } ;
    ?>
    </div>

    <?php include "footer.php" ?>

</body>
</html>