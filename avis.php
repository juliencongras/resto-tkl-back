<?php include 'H4.php';
include 'header3.php';?>

<!--le lien ci dessous est FONT AWESOME pour la notation étoile-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../style.css">
<title>Avis</title>
<body>
<main>

        <form class="avis zone" action="avisC.php" method="post">
              <div class="formulaire">
                  <div class="entree">
                    <input type="text" class="noms" name ="Aname">
                    <input type="submit" onclick="avis()" value="Publier">
                  </div>
                  
                  <div class="etoile stars">
                    <input type='radio' class="fa fa-star gold" name="star" value='1'>
                    <input type='radio' class="fa fa-star gold" name="star" value='2'>
                    <input type='radio' class="fa fa-star " name="star" value='3'>
                    <input type='radio' class="fa fa-star " name="star" value='4'>
                    <input type='radio' class="fa fa-star " name="star" value='5'>
                   </div>
                  
                  </div>
                  <textarea class="texte" name='Atext'></textarea>
        </form>

                <!-- ci-dessous la div finale qui affiche les avis           -->
<?php for($i = 0; $i < count($avis) ;$i++){?>       
         <div class="avis">
              <div class="formulaire">
                  <div class="entree">
                    <div class="noms2"><?php echo $avis[$i]["nom"];?></div>
                  </div>
                  <div class="etoile">
                    <?php
                      $c = 0;
                      while($c++ < 5){
                        if($c <= $avis[$i]["etoile"]){
                          $gold = ' gold';
                        }else{ 
                          $gold = "";
                        }
                        echo "<i class='fa fa-star$gold'></i>";
                      }
                    ?>
                  </div>
                  </div>
                <div class="texte"><?php echo $avis[$i]["textavis"];?></div>
               
    </div>
    <?php } ?>
    </main> 

    <?php include 'footer.php'?>
                    
</body>