<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu</title>
    <link rel="stylesheet" href="../../TKL/resto-tkl/style.css">

</head>
<body>
    <?php include "H2.php";
    include 'header3.php';?>
    <?php include 'data.php';?>
    
    <?php session_start();

    if($_SESSION['admin']==1){
        ?>
    <a href="bcreateformu.php"> + </a>

    <?php } ?>
    <?php for($i=0; $i < count($burger); $i++){
    ?>
    
    <div class="containermenu">
    <div>
        <?php $checked = $burger[$i]['dispo'];?>
        <img src="<?php echo $burger[$i]['imgurl']; ?>" class="imgmenu">
        <div class="divprix">
        <input type="image" disabled name='dispo' 
            <?php  
        if($checked){echo ("<img src='image/checked.png'>");} 
        else {echo ("<img src='image/notCheked.png'>");} ?> >

            <p class="prix"><?php echo $burger[$i]['prix']; ?> € </p>
        </div>
        
    </div>
    <div class="textmenu">
        <h3><?php echo $burger[$i]['nom']; ?></h3>
        <p><?php echo $burger[$i]['descri']; ?>
        
    </div>
    <?php
    if($_SESSION['admin']){
        echo "<a href='formulaireAdmin.php?id=".$burger[$i]['id']."'>modifier</a>";
    } ;
    ?>
    </div>

    <?php } ?>

    <?php include "footer.php"?>
</body>
</html>

