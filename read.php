<?php
include_once "model.php";

$avis = readAll();

foreach($avis as $avi){
    ?>
    <?php for($i = 0; $i < count($avis) ;$i++){?>       
         <div class="avis">
              <div class="formulaire">
                  <div class="entree">
                    <div class="noms2"><?php echo $avis[$i]["nom"];?></div>
                  </div>
                  <div class="etoile">
                    <?php
                      $c = 0;
                      while($c++ < 5){
                        if($c <= $avis[$i]["etoile"]){
                          $gold = ' gold';
                        }else{ 
                          $gold = "";
                        }
                        echo "<i class='fa fa-star$gold'></i>";
                      }
                    ?>
                  </div>
                  </div>
                <div class="texte"><?php echo $avis[$i]["textAvis"];?></div>
               
    </div>
    <?php } ?>
    <p>
        <a href="delete.php?id=<?= $avi['id'] ?>">suppr</a>
        <a href="?id=<?= $avi['id'] ?>">modif</a>
    </p>
    <?php
}

$burger = readAll();

foreach($burger as $burg){
    ?>
    <div class="containermenu">
    <div>
        <img src="<?php echo $burger['imgurl']; ?>" class="imgmenu">
        <div class="divprix">
            <img src="<?php echo $burger['dispo']; ?>" class="imgcheckmark"><p class="prix"><?php echo $burg['prix']; ?></p>
        </div>
    </div>
    <div class="textmenu">
        <h3><?php echo $burger['nom']; ?></h3>
        <p><?php echo $burger['descri']; ?></p>
    </div>
    </div>
    <p>
        <a href="delete.php?id=<?= $burg['id'] ?>">suppr</a>
        <a href="?id=<?= $burg['id'] ?>">modif</a>
    </p>
    <?php
}

$contact = readAll();

foreach($contact as $cont){
    ?>
    <div class="container">
        <div class="text"><p>Adresse :<span class="center"><?=$cont['adresse']?></span></p>
            <p>Télephone : <a href="tel:<?=$cont['tel']?>" class="center"><?=$cont['tel']?></a></p>
        </div>
        <iframe src=<?=$cont['mapURL']?> width="450" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
    <p>
        <a href="delete.php?id=<?= $cont['id'] ?>">suppr</a>
        <a href="?id=<?= $cont['id'] ?>">modif</a>
    </p>
    <?php
}

foreach($horaire as $hor){
    ?>
    <div class="horaires">
    <div class="content">
        <p> Fermé tous les <?php echo$jour["L"]?></p>
    </div>
    <div class="content-container">
    <div class="other">
    <div class="other-content">
    <p><?php echo$jour["MV"] ?></p>
    </div>
    <div class="other-content">
    <p><?php echo$jour["SD"] ?></p>
    </div>
    <div class="other-content">
    <p><?php echo$jour["V"] ?></p>
    </div>
    </div>
    <div class="other">
    <div class="other-content">
    <p><?php echo$ouvertmatin["1"]?></p>
    
    <p><?php echo$fermematin["1"]?></p>
    </div>
    <div class="other-content">
    <p><?php echo$ouvertsoir["2"]?></p>
    <p><?php echo$fermesoir["2"]?></p>
    </div>
    <div class="other-content">
    <p><?php echo$fermesoir["2"]?></p>
    </div>
    </div>
    </div>
    <div class="content">
        <p> <?php echo$jour["V"]?></p>
    </div>
</div>
    <p>
        <a href="delete.php?id=<?= $hor['id'] ?>">suppr</a>
        <a href="?id=<?= $hor['id'] ?>">modif</a>
    </p>
    <?php
}

foreach($pres as $pre){
    ?>
    <div class="main" style="overflow:auto; ">
    <div class="title">
        Qui Sommes nous ? 
    </div>
    <div class="paragraphe-left"> 
    <img src="<?php echo$pre["imgURL1"];?>">
    <p><?php echo$pre["txt1"];?></p>
    </div>

    <div class="paragraphe-right">
    <img src="<?php echo$pre["imgURL2"];?>">
    <p><?php echo$pre["txt2"];?></p>
    </div>

    <div class="paragraphe-left"> 
    <img src="<?php echo$pre["imgURL3"];?>">
    <p><?php echo$pre["txt3"];?></p>
    </div>
    
    <div class="paragraphe-right">
    <img src="<?php echo$pre["imgURL4"];?>">
    <p><?php echo$pre["txt4"];?></p>
    </div>
</div>

    <p>
        <a href="delete.php?id=<?= $pre['id'] ?>">suppr</a>
        <a href="?id=<?= $pre['id'] ?>">modif</a>
    </p>
    <?php
}