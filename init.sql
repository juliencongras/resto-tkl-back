
drop database tkl;
create database tkl;
use tkl;
create table pres(
    id int not null auto_increment primary key,
    txt1 varchar(512) not null,
    txt2 varchar(512) not null,
    txt3 varchar(512) not null,
    txt4 varchar(512) not null,
    imgURL1 varchar(512),
    imgURL2 varchar(512),
    imgURL3 varchar(512),
    imgURL4 varchar(512)
    );
create table burger(id int not null auto_increment primary key,imgurl varchar(512),nom varchar(255),descri varchar(255),prix float,dispo boolean);
create table horaire(
    id int not null auto_increment primary key,
    jour varchar(16),
    ouvertmatin varchar(255),
    fermematin varchar(255),
    ouvertsoir varchar(255),
    fermesoir varchar(255)
    );
create table contact(id int not null auto_increment primary key,tel varchar(255),adresse varchar(255),mapURL varchar(512));
create table avis(id int not null auto_increment primary key, etoile int not null,nom varchar(255) not null,textavis varchar(255));
drop USER 'admin'@'127.0.0.1';
CREATE USER 'admin'@'127.0.0.1' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON tkl.* TO 'admin'@'127.0.0.1';
