<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>FormulaireAdmin</title>
</head>
<body>
    <?php include 'H5.php';
    include "header3.php";?>
    <?php include 'data.php'?>
    <?php session_start();?>
                
    <form class="container" action="update-cont.php" method="post">
        <div class="text"><p>Adresse :<input type="text" name="c_adresse" value='<?php echo $cont[0]['adresse']?>'></p>
            <p>Télephone : <input type="text" name="c_tel" value='<?php echo $cont[0]['tel']?>'></p>
            <p>Carte : <input type="text" name="c_mapURL" value='<?php echo $cont[0]['mapURL']?>'></p>
            <div class="bouton">
                <input type="submit" name="modifier" value='modifier'>
            </div>
        </div>
        <iframe src=<?=$cont[0]['mapURL']?> width="450" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </form>
    
    <?php include "footer.php"?>
            
</body>
</html>