<?php 
$host = '127.0.0.1';
$db   = 'tkl';
$user = 'admin';
$pass = 'admin';

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);

function readAll($tab){
    global $pdo;
    $req = $pdo->query("SELECT*FROM $tab ;");
    return $req->fetchAll();
}


// =======================
function Pread($id){
    global $pdo;
    $req = $pdo->prepare("select * from pres where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
}
function Pupdate($id, $txt1,$txt2,$txt3,$txt4,$imgURL1,$imgURL2,$imgURL3,$imgURL4){
    global $pdo;
    $req = $pdo->prepare("update pres set txt1=?, txt2=?, txt3=?, txt4=?, imgURL1=?, imgURL2=?, imgURL3=?, imgURL4=? where id=?;");
    $req->execute([$txt1,$txt2,$txt3,$txt4,$imgURL1,$imgURL2,$imgURL3,$imgURL4,$id]);
}
// =======================

function Bcreate($imgURL, $nom, $descri, $prix, $dispo){
    global $pdo;
    $req = $pdo->prepare('insert into burger (imgurl,nom,descri,prix,dispo) value (?, ?, ? , ? , ? );');
    $req->execute([$imgURL, $nom, $descri, $prix, $dispo]);
}
function Bread($id){
    global $pdo;
    $req = $pdo->prepare("select * from burger where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
}
function Bupdate($id,$dispo,$imgURL, $nom, $descri, $prix){
    global $pdo;
    $req = $pdo->prepare("update burger set imgurl=?, nom=?, descri=?, prix=?, dispo=? where id=?;");
    $req->execute([$imgURL, $nom, $descri, $prix, $dispo, $id]);
}
function Bdelete($id){
    global $pdo;
    $req = $pdo->prepare("delete from burger where id=?;");
    $req->execute([$id]);
}
// =======================

function Hread($id){
    global $pdo;
    $req = $pdo->prepare("select * from horaire where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
}
function Hupdate($id,$om,$fm,$os,$fs){
    global $pdo;
    $req = $pdo->prepare("update horaire set ouvertmatin=?,fermematin=?,ouvertsoir=?,fermesoir=? where id=?;");
    $req->execute([$om,$fm,$os,$fs,$id]);
}

// ==========================
function Aread($id){
    global $pdo;
    $req = $pdo->prepare("select * from avis where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
}
function Acreate($etoile,$nom,$textavis){
    global $pdo;
    $req = $pdo->prepare('insert into avis (etoile, nom, textavis) value (?, ?, ? );');
    $req->execute([$etoile, $nom, $textavis]);
}




function Cread($id){
    global $pdo;
    $req = $pdo->prepare("select * from contact where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
}
function Cupdate($id,$adresse,$tel,$mapURL){
    global $pdo;
    $req = $pdo->prepare("update contact set adresse=?,tel=?,mapURL=? where id=?;");
    $req->execute([$adresse,$tel,$mapURL,$id]);
}





?>